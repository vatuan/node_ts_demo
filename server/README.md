## Setup TS with Nodejs

- tạo 1 folder tên : `TS_Node_Example` để bắt đầu
- cd đến `TS_Node_Example`, chạy câu lệnh : `npm init -y` để tạo file đầu tiên là : `package.json`
- `yarn add -D typescript` để tạo môi trường devDependencies với TS

```php
  "devDependencies": {
    "typescript": "^4.0.5"
  }
```

- tạo file `tsconfig.json` bằng cách : `tsc --init`, file này chứa các cấu hình của TS
- tiếp theo : `yarn add -D ts-node`,
- tại file `package.json`:

```php
  "start": "ts-node src/index.ts",
```

> Nhưng nếu chỉ sủ dụng ts-node thì mỗi lần có thay đổi lại phải chạy `yarn start` khá dài dòng, để khắc phục, xem tiếp bên dưới :

- chạy `yarn add -D ts-node-dev`

```php
 "devDependencies": {
    "ts-node": "^9.0.0",
    "ts-node-dev": "^1.0.0",
    "typescript": "^4.0.5"
  }
```

- thay đổi lại file `package.json` :

```php
++ "start": "ts-node-dev --respawn src/index.ts",
```

##### Một cách khác để làm nữa đó là add thêm nodemon

- chạy : `yarn add -D nodemon`

- tại nodemon.json :

```php
{
    "restartable": "rs",
    "ignore": [".git", "node_modules/**/node_modules"],
    "verbose": true,
    "execMap": {
      "ts": "node --require ts-node/register"
    },
    "watch": ["src/"],
    "env": {
      "NODE_ENV": "development"
    },
    "ext": "js,json,ts"
  }
```

> Cuối cùng file `package.json` sẽ như thế này :

```php
{
  "name": "Node_TS_Demo",
  "version": "1.0.0",
  "main": "index.js",
  "license": "MIT",
  "scripts": {
    "start": "nodemon src/index.ts",
    "dev" : "nodemon --inspect src/index.ts"
  },
  "devDependencies": {
    "@types/express": "^4.17.8",
    "nodemon": "^2.0.6",
    "ts-node": "^9.0.0",
    "typescript": "^4.0.5"
  },
  "dependencies": {
    "express": "^4.17.1"
  }
}

```

- cài đặt express :
- `yarn add express`
- `yarn add -D @types/node @types/express`

#### Cài đặt thư viện morgan

- mục đích là để quan sát những request từ client lên serve
- nguyên văn : `HTTP request logger middleware for node.js`
-

### Cách kill PORT đang chạy :

- mở powershell với quyền admintrator
- step 1 : `netstat -ano | findstr :<Nhập_PORT_Đang_Sử_Dung>`, ví dụ : netstat -ano | findstr :2908
- step 2 : `taskkill /PID <PID> /F` , ví dụ : taskkill /PID 12450 /F, cái số này là lấy số sau chữ LISTENING
